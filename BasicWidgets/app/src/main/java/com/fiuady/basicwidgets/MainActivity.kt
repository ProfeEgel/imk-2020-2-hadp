package com.fiuady.basicwidgets

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    private val flavorsCheckboxes = mutableListOf<CheckBox>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        flavorsCheckboxes.add(findViewById(R.id.chocolate_checkbox))
        flavorsCheckboxes.add(findViewById(R.id.fresa_checkbox))
        flavorsCheckboxes.add(findViewById(R.id.menta_checkbox))
        flavorsCheckboxes.add(findViewById(R.id.chicle_checkbox))
        flavorsCheckboxes.add(findViewById(R.id.chocochip_checkbox))

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { _ ->
            Toast
                .makeText(this, "¡¡FLOATING!!", Toast.LENGTH_SHORT)
                .show()
        }

        //val planetsRadio: RadioButton = findViewById(R.id.mercurio_radio)
        //planetsRadio.isChecked = true

        //val chocolateCheckbox : CheckBox = findViewById(R.id.chocolate_checkbox)
        //chocolateCheckbox.setOnCheckedChangeListener{ _, isChecked ->
        //    if (isChecked) {
        //        Toast.makeText(this, "CHOCOLATE", Toast.LENGTH_SHORT).show()
        //    }
        //}
    }

    fun onFlavorsChanged(view: View) {
        val flavorsList = mutableListOf<String>()
        flavorsCheckboxes.forEach {
            if (it.isChecked) {
                flavorsList.add(it.text.toString())
            }
        }

        val flavorsText: EditText = findViewById(R.id.flavors_text)
        flavorsText.setText(flavorsList.joinToString())

        //val flavorCheckbox = view as CheckBox
        //if (flavorCheckbox.isChecked) {
        //    Toast.makeText(this, flavorCheckbox.text, Toast.LENGTH_SHORT).show() }
    }

    fun onPlanetsChanged(view: View) {
        val planetRadio = view as RadioButton

        val planetsText: EditText = findViewById(R.id.planets_text)
        planetsText.setText(planetRadio.text)

        if (planetRadio.text == "Tierra") {
            Snackbar
                .make(view, "Option selected", Snackbar.LENGTH_INDEFINITE)
                .setAnimationMode(Snackbar.ANIMATION_MODE_FADE)
                .setAction("Deshacer", View.OnClickListener {
                    Snackbar
                        .make(view, "Opción cancelada", Snackbar.LENGTH_SHORT)
                        .show()
                })
                //.setActionTextColor(Color.RED)
                .setActionTextColor(Color.rgb(255, 255, 0))
                .show()
        }
    }
}
