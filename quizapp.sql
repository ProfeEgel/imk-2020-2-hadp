﻿
DROP TABLE IF EXISTS [options];
DROP TABLE IF EXISTS [themes];

CREATE TABLE [themes](
  [id] INTEGER PRIMARY KEY, 
  [title] TEXT NOT NULL UNIQUE);

CREATE TABLE [options](
  [user_id] INTEGER PRIMARY KEY, 
  [num_of_questions] INTEGER NOT NULL DEFAULT 5, 
  [difficulty] INTEGER NOT NULL DEFAULT 0, 
  [hints_enabled] INTEGER NOT NULL DEFAULT 0, 
  [num_of_hints] INTEGER NOT NULL DEFAULT 1, 
  CHECK(num_of_questions >= 5 AND num_of_questions <= 10),
  CHECK(difficulty >= 0 AND difficulty <= 2),
  CHECK(hints_enabled = 0 OR hints_enabled = 1),
  CHECK(num_of_hints > 0 AND num_of_hints <= 3));


INSERT INTO themes(id, title) VALUES(0, 'Cine');
INSERT INTO themes(id, title) VALUES(1, 'Historia');
INSERT INTO themes(id, title) VALUES(2, 'Física');
INSERT INTO themes(id, title) VALUES(3, 'Matemáticas');
INSERT INTO themes(id, title) VALUES(4, 'Arte');
INSERT INTO themes(id, title) VALUES(5, 'Deportes');

INSERT INTO options(user_id) VALUES (0);
INSERT INTO options(user_id) VALUES (1);

-- UPDATE options SET num_of_questions=12 WHERE user_id = 1;

SELECT * FROM themes;
SELECT title FROM themes;
SELECT id, title, title FROM themes;

SELECT * FROM themes WHERE id = 1;
SELECT * FROM themes WHERE id >= 1 AND id <= 3;
SELECT * FROM themes WHERE id = 1 OR id = 3;
SELECT * FROM themes WHERE id IN (1, 3, 5);
SELECT * FROM themes WHERE id NOT IN (1, 3, 5);

SELECT * FROM themes WHERE title = 'Cine';
SELECT * FROM themes WHERE title = 'cine';
SELECT * FROM themes WHERE title LIKE 'cine';
SELECT * FROM themes WHERE title LIKE '%a%';
SELECT * FROM themes WHERE title LIKE 'a%';
SELECT * FROM themes WHERE title LIKE '%a';
SELECT * FROM themes WHERE title NOT LIKE 'a%' AND title NOT LIKE '%a';

SELECT * FROM themes ORDER BY title;
SELECT * FROM themes ORDER BY title ASC;
SELECT * FROM themes ORDER BY title DESC;

SELECT * FROM themes LIMIT 2;
SELECT * FROM themes LIMIT 2 OFFSET 2;
SELECT * FROM themes LIMIT 2 OFFSET 4;

SELECT * FROM themes ORDER BY RANDOM();

 
questions
question_id   text              theme_id   answered pos
0             ¿La luna es...?   0          2        3

question_answers
question_id   answer    true  pos hint_used
0             aafadfad  0     0   1
0             mmmfsdfw  0     1   0
0             frg5rggf  1     2   1
0             4mf45543  0     3   0

