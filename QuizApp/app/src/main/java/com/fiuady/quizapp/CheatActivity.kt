package com.fiuady.quizapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class CheatActivity : AppCompatActivity() {

    companion object {
        const val RESULT_CHEATED = RESULT_FIRST_USER;

        const val EXTRA_QUESTION_TEXT = "com.fiuady.quizapp.question_text"
        const val EXTRA_QUESTION_ANSWER = "com.fiuady.quizapp.question_answer"
        const val EXTRA_CHEATED_MESSAGE = "com.fiuady.quizapp.cheated_message"

        fun createIntent(packageContext: Context, questionId: Int, answer: Boolean): Intent {
            return Intent(packageContext, CheatActivity::class.java).apply {
                putExtra(EXTRA_QUESTION_TEXT, questionId)
                putExtra(EXTRA_QUESTION_ANSWER, answer)
            }
        }
    }

    private lateinit var questionTextView: TextView
    private lateinit var cheatButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cheat)

        questionTextView = findViewById(R.id.question_text)
        cheatButton = findViewById(R.id.cheat_button)

        val questionText = intent.getIntExtra(EXTRA_QUESTION_TEXT, -1)
        val questionAnswer = intent.getBooleanExtra(EXTRA_QUESTION_ANSWER, false)

        questionTextView.setText(questionText)

        cheatButton.setOnClickListener { _ ->
            setResult(RESULT_CHEATED, Intent().apply {
                putExtra(EXTRA_CHEATED_MESSAGE, "Cheated :(")
            })
        }
    }
}
