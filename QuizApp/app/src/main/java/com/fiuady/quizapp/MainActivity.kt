package com.fiuady.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels

// MVC
// Model-View-Controller

// MVVM
// Model-View-ViewModel

private const val DEBUG_TAG = "LIFECYCLE_DEBUG"
private const val CHEAT_ACTIVITY_REQUEST_CODE = 0;

class MainActivity : AppCompatActivity() {

    private lateinit var questionText: TextView
    private lateinit var falseButton: Button
    private lateinit var trueButton: Button
    private lateinit var nextButton: Button
    private lateinit var prevButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d(DEBUG_TAG, "onCreate()...")

        setContentView(R.layout.activity_main)

        val gameModel: GameModel by viewModels()

        questionText = findViewById(R.id.question_text)
        falseButton = findViewById(R.id.false_button)
        trueButton = findViewById(R.id.true_button)
        nextButton = findViewById(R.id.next_button)
        prevButton = findViewById(R.id.prev_button)

        questionText.setText(gameModel.currentQuestion.resId)
        questionText.setOnClickListener { _ ->
            startActivityForResult(
                CheatActivity.createIntent(
                    this,
                    gameModel.currentQuestion.resId,
                    gameModel.currentQuestion.answer
                ),
                CHEAT_ACTIVITY_REQUEST_CODE
            )
        }

        falseButton.setOnClickListener { _ ->
            Toast.makeText(
                this,
                if (!gameModel.currentQuestion.answer) R.string.correct_text else R.string.incorrect_text,
                Toast.LENGTH_SHORT
            ).show()
        }

        trueButton.setOnClickListener { _ ->
            Toast.makeText(
                this,
                if (gameModel.currentQuestion.answer) R.string.correct_text else R.string.incorrect_text,
                Toast.LENGTH_SHORT
            ).show()
        }

        nextButton.setOnClickListener { _ ->
            gameModel.nextQuestion()
            questionText.setText(gameModel.currentQuestion.resId)
        }

        prevButton.setOnClickListener { _ ->
            gameModel.previousQuestion()
            questionText.setText(gameModel.currentQuestion.resId)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CHEAT_ACTIVITY_REQUEST_CODE ->
                Toast.makeText(this,
                    when (resultCode) {
                        RESULT_OK -> "CHEATER..."
                        RESULT_CANCELED -> "OK, GOOD..."
                        CheatActivity.RESULT_CHEATED -> data!!.getStringExtra(CheatActivity.EXTRA_CHEATED_MESSAGE)
                        else -> "WHAT??"
                    },
                    Toast.LENGTH_SHORT).show()
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(DEBUG_TAG, "onStart()...")
    }

    override fun onResume() {
        super.onResume()
        Log.d(DEBUG_TAG, "onResume()...")
    }

    override fun onPause() {
        super.onPause()
        Log.d(DEBUG_TAG, "onPause()...")
    }

    override fun onStop() {
        super.onStop()
        Log.d(DEBUG_TAG, "onStop()...")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(DEBUG_TAG, "onDestroy()...")
    }
}
