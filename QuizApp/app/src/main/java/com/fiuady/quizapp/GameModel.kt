package com.fiuady.quizapp

import androidx.lifecycle.ViewModel

class GameModel : ViewModel() {
    private val questions = listOf<Question>(
        Question(R.string.question_text_1, true),
        Question(R.string.question_text_2, false),
        Question(R.string.question_text_3, true),
        Question(R.string.question_text_4, false),
        Question(R.string.question_text_5, false),
        Question(R.string.question_text_6, true),
    )

    private var currentQuestionIndex = 0

    val currentQuestion : Question
        get() = questions[currentQuestionIndex]

    fun nextQuestion() {
        currentQuestionIndex = (currentQuestionIndex + 1) % questions.size;
    }

    fun previousQuestion() {
        currentQuestionIndex = (questions.size + currentQuestionIndex - 1) % questions.size;
    }
}
