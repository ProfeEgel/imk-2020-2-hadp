package com.fiuady.quizapp

data class Question(val resId: Int, val answer: Boolean)
