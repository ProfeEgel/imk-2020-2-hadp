package com.fiuady.recyclerviewdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text

class EmployeesAdapter(private val employeesArray: Array<Employee>) :
    RecyclerView.Adapter<EmployeesAdapter.EmployeeHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class EmployeeHolder(val view: View) : RecyclerView.ViewHolder(view)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.rv_item, parent, false)
        return EmployeeHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: EmployeeHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.full_name_text).text =
            employeesArray[position].fullName

        holder.view.findViewById<CheckBox>(R.id.active_checkbox).isChecked =
            employeesArray[position].active
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = employeesArray.size
}
