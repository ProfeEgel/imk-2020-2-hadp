package com.fiuady.recyclerviewdemo

data class Employee(val fullName: String, val active: Boolean)
