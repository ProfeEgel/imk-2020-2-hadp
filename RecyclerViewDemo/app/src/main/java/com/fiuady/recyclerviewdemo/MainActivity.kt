package com.fiuady.recyclerviewdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val employeesArray = arrayOf(
            Employee("Pepe", true),
            Employee("Javier", true),
            Employee("Erika", false),
            Employee("Pedro", true),
            Employee("Egel", false),
            Employee("Mauro", true),
            Employee("Elizabeth", false),
            Employee("Miguel", false),
            Employee("Abelardo", true),
            Employee("Karla", true),
            Employee("Eduardo", true),
            Employee("Hutch", false)
        )
        viewAdapter = EmployeesAdapter(employeesArray)

        recyclerView = findViewById<RecyclerView>(R.id.rv).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = LinearLayoutManager(this@MainActivity)

            // specify an viewAdapter
            adapter = viewAdapter
        }

    }
}
