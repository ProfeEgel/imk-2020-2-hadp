package com.fiuady.roomdemo.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Theme::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun themesDao(): ThemesDao
}
