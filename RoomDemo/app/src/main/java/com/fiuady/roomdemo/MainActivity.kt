package com.fiuady.roomdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.facebook.stetho.Stetho
import com.fiuady.roomdemo.db.AppDatabase

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Stetho.initializeWithDefaults(this);

        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "quizapp.db"
        ).allowMainThreadQueries()
            .addCallback(object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)

                    db.execSQL("INSERT INTO themes(id, title) VALUES(0, 'Cine')")
                    db.execSQL("INSERT INTO themes(id, title) VALUES(1, 'Historia')")
                    db.execSQL("INSERT INTO themes(id, title) VALUES(2, 'Física')")
                    db.execSQL("INSERT INTO themes(id, title) VALUES(3, 'Matemáticas')")
                    db.execSQL("INSERT INTO themes(id, title) VALUES(4, 'Arte')")
                    db.execSQL("INSERT INTO themes(id, title) VALUES(5, 'Deportes')")
                }
            }).build()

        //val themes = db.themesDao().getThemes()
        //val themes = db.themesDao().getThemes("%i%a%")
        //val themes = db.themesDao().getThemesByIdArray(arrayOf(1, 3, 5))

        val dao = db.themesDao()
        val theme = dao.getTheme(3).apply { title = "Series" }
        dao.updateTheme(theme)

        //dao.deleteTheme(dao.getTheme(1))

        val themes = dao.getThemes()
    }
}
