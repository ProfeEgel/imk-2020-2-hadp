package com.fiuady.roomdemo.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "themes",
    indices = [Index(value = ["title"], unique = true)]
)
data class Theme(
    @PrimaryKey @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "title") var title: String
)
