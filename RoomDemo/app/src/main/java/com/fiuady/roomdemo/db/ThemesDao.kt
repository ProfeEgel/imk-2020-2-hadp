package com.fiuady.roomdemo.db

import androidx.room.*

@Dao
interface ThemesDao {
    @Query("SELECT * FROM themes ORDER BY id")
    fun getThemes(): List<Theme>

    @Query("SELECT * FROM themes WHERE id = :id")
    fun getTheme(id: Int): Theme

    @Query("SELECT * FROM themes WHERE title LIKE :text ORDER BY title")
    fun getThemes(text: String): List<Theme>

    @Query("SELECT * FROM themes WHERE id >= :min AND id <= :max")
    fun getThemesByIdRange(min: Int, max: Int): List<Theme>

    @Query("SELECT * FROM themes WHERE id IN (:idArray)")
    fun getThemesByIdArray(idArray: Array<Int>): List<Theme>

    @Update
    fun updateTheme(theme: Theme)

    @Update
    fun updateTheme(theme1: Theme, theme2: Theme)

    @Update
    fun updateThemes(themes: List<Theme>)

    @Insert
    fun insertTheme(theme: Theme)

    @Insert
    fun insertTheme(theme1: Theme, theme2: Theme)

    @Insert
    fun insertThemes(themes: List<Theme>)

    @Delete
    fun deleteTheme(theme: Theme)

    @Delete
    fun deleteTheme(theme1: Theme, theme2: Theme)

    @Delete
    fun deleteThemes(themes: List<Theme>)
}
